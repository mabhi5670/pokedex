import styles from './Clickable.module.css';

const ClickableDiv = ({ count, onClickHandler }) => {
    return (
        <div 
            className={styles.clickableDiv}
            onClick={onClickHandler}
        >
            <span>The other div was clicked {count} times</span>
        </div>
    )
}

export default ClickableDiv;
