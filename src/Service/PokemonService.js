const pokemons = [
    {
      name: "Togepi",
      isSeen: false,
      info: "Togepi info",
    },
    {
      name: "Charizard",
      isSeen: false,
      info: "Charizard info",
    },
    {
      name: "Pikachu",
      isSeen: false,
      info: "Pikachu info",
    },
    {
      name: "Charmander",
      isSeen: false,
      info: "Charmander info",
    },
    {
      name: "Bulbasaur",
      isSeen: true,
      info: "Bulbasaur info",
    },
    {
      name: "Squirtle",
      isSeen: false,
      info: "Squirtle info",
    },
  ];
  
  export const getPokemons = () => pokemons;