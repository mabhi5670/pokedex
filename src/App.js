// import { useState } from "react";
// import ClickableDiv from "./components/ClickableDiv/ClickableDiv";

// const App = () => {

//     const [count1, setCount1] = useState(0);
//     const [count2, setCount2] = useState(0);

//     const clickHandler = (state, value) => {
//         state(value + 1);
//     }

//     return (
//         <>
//             <ClickableDiv 
//                 count={count1}
//                 onClickHandler={() => {
//                     clickHandler(setCount2, count2)
//                 }}
//             />

//             <ClickableDiv 
//                 count={count2}
//                 onClickHandler={() => {
//                     clickHandler(setCount1, count1)
//                 }}
//             />
//         </>
//     );
// }

// export default App;




import Layout from "./components/Layout/Layout";

const App = () => <Layout />;

export default App;
